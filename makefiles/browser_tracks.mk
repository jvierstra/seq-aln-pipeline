
###############
# These variables should be passed in for the makefile to work properly
###############
# INPUT_CONTIG_SIZES_FILE=
# INPUT_CUTCOUNTS_FILE=
# OUTPUT_DENSITY_FILE=
# OUTPUT_DENSITY_BIGWIG_FILE=
###############

TMPDIR ?= $(shell pwd)

NSLOTS ?= 1
THREADS ?= $(NSLOTS)

DEFAULT_MEM ?= 4
MAX_MEM ?= 4

WINDOW_WIDTH ?= 150
WINDOW_STEP_SIZE ?= 20
WINDOW_RANGE := $(shell echo \(${WINDOW_WIDTH} / 2\) - \(${WINDOW_STEP_SIZE} / 2\) | bc)

NORMALIZATION ?= 1000000
TOTAL_TAGS := $(shell bedops -u $(INPUT_CUTCOUNTS_FILE) | awk '{ t += $$5; } END { print t; }')


all: density

density: $(OUTPUT_DENSITY_FILE) $(OUTPUT_DENSITY_BIGWIG_FILE)

$(OUTPUT_DENSITY_BIGWIG_FILE): $(TMPDIR)/density.bw
	rsync $^ $@

$(OUTPUT_DENSITY_FILE): $(TMPDIR)/density.bed
	starch $^ > $@

$(TMPDIR)/density.bw: $(TMPDIR)/density.wig $(INPUT_CONTIG_SIZES_FILE)
	wigToBigWig -clip $(TMPDIR)/density.wig $(INPUT_CONTIG_SIZES_FILE) $@

$(TMPDIR)/density.wig: $(TMPDIR)/density.bed
	cat $^ \
		| awk \
			-v window_range=$(WINDOW_RANGE) \
			-v window_step_size=$(WINDOW_STEP_SIZE) \
			'chr != $$1 { \
				print "fixedStep chrom="$$1" start="window_range" step="window_step_size" span="window_step_size; \
				chr=$$1; \
			} \
			{ print $$5; }' \
	> $@

$(TMPDIR)/density.bed: $(INPUT_CONTIG_SIZES_FILE) $(INPUT_CUTCOUNTS_FILE) 
	cat $(INPUT_CONTIG_SIZES_FILE) | awk -v OFS="\t" '{ print $$1, "0", $$2; }' | sort-bed --max-mem $(MAX_MEM)G - \
		| awk -v OFS="\t" \
			-v window_width=$(WINDOW_WIDTH) \
			-v window_step_size=$(WINDOW_STEP_SIZE) \
			'{ \
				for(i = 0; i < $$3-window_width; i += window_step_size) { \
					print $$1, i, i + window_width, "i"; \
				} \
			}' \
		| bedmap --delim "\t" --faster --echo --sum - $(INPUT_CUTCOUNTS_FILE) \
		| awk -v OFS="\t" \
			-v window_range=$(WINDOW_RANGE) \
			-v normalization=$(NORMALIZATION) \
			-v total_tags=$(TOTAL_TAGS) \
			'{ \
				$$2 += window_range; \
				$$3 -= window_range; \
				$$5 = ($$5 / total_tags) * normalization; \
				print; \
			}' \
	> $@