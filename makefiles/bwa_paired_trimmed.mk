
###############
# These variables should be passed in for the makefile to work properly
###############
# SCRIPT_DIR=
# ADAPTER_FILE=
# ADAPTER1=
# ADAPTER2=
# FASTQ_READ1_FILE=
# FASTQ_READ2_FILE=
# TRIM_STATS_FILE=
# BWA_INDEX=
# OUTPUT_BAM_FILE=
###############
# Optional variables
###############
# UMI=
# NSLOTS=
# TMPDIR=

SHELL = bash

TMPDIR ?= $(shell pwd)

NSLOTS ?= 1
THREADS ?= $(NSLOTS)

DEFAULT_MEM ?= 4
MAX_MEM ?= 4


BWA_ALN_OPTIONS ?= -t $(THREADS) -Y -n 0.04 -l 32
BWA_SAMPE_OPTIONS ?= -a 750

UMI ?= 0

ifeq ($(UMI), 1)
	prefix = trimmed-with-umi
else
	prefix = trimmed
endif


all : align

align : $(OUTPUT_BAM_FILE)

$(OUTPUT_BAM_FILE): $(TMPDIR)/reads.sorted.bam
	rsync $^ $@

# Convert to BAM and sort
$(TMPDIR)/reads.sorted.bam: $(TMPDIR)/reads.sam
	samtools view -Sbu $^ | samtools sort -@ $(THREADS) -m $(DEFAULT_MEM)G -n - $(basename $@)

# Pair reads...
$(TMPDIR)/reads.sam: $(TMPDIR)/$(prefix).R1.sai $(TMPDIR)/$(prefix).R2.sai $(TMPDIR)/$(prefix).R1.fastq.gz $(TMPDIR)/$(prefix).R2.fastq.gz
	bwa sampe $(BWA_SAMPE_OPTIONS) $(BWA_INDEX) $^ > $@
 
# Run first pass alignment...
$(TMPDIR)/$(prefix).%.sai: $(TMPDIR)/$(prefix).%.fastq.gz
	bwa aln $(BWA_ALN_OPTIONS) $(BWA_INDEX) $(TMPDIR)/$(prefix).$*.fastq.gz > $(TMPDIR)/$(prefix).$*.sai

# If UMI present, put it in the read name
$(TMPDIR)/trimmed-with-umi.%.fastq.gz: $(TMPDIR)/trimmed.%.fastq.gz
	python $(SCRIPT_DIR)/scripts/reorganize_umi.py $(TMPDIR)/trimmed.$*.fastq.gz $(TMPDIR)/trimmed-with-umi.$*.fastq.gz

# Trim adapters...
$(TMPDIR)/trimmed.R1.fastq.gz $(TMPDIR)/trimmed.R2.fastq.gz: $(FASTQ_READ1_FILE) $(FASTQ_READ2_FILE)
	/home/jvierstra/proj/code/bio-tools/apps/trim-adapters-illumina/trim-adapters-illumina \
		-@$(THREADS) \
		-f $(ADAPTER_FILE) -1 $(ADAPTER1) -2 $(ADAPTER2) \
		$(FASTQ_READ1_FILE) $(FASTQ_READ2_FILE) \
		$(TMPDIR)/trimmed.R1.fastq.gz $(TMPDIR)/trimmed.R2.fastq.gz \
	&> $(TRIM_STATS_FILE) >&2

