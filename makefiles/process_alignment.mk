
###############
# These variables should be passed in for the makefile to work properly
###############
# INPUT_BAM_FILE=
# OUTPUT_FRAGMENTS_FILE=
# OUTPUT_CUTCOUNTS_FILE=
###############

TMPDIR ?= $(shell pwd)

NSLOTS ?= 1
THREADS ?= $(NSLOTS)

DEFAULT_MEM ?= 4
MAX_MEM ?= 4

SAMTOOLS_FILTER_OPTIONS ?= -f 2 -F 512 -F 1024 

all: $(OUTPUT_FRAGMENTS_FILE) $(OUTPUT_CUTCOUNTS_FILE)

$(OUTPUT_CUTCOUNTS_FILE): $(TMPDIR)/cleavages.per-base.bed
	starch $^ > $@

$(OUTPUT_FRAGMENTS_FILE): $(TMPDIR)/fragments.sorted.bed
	starch $^ > $@

$(TMPDIR)/cleavages.per-base.bed: $(TMPDIR)/cleavages.sorted.bed
	bedops -m $^ | bedops --chop 1 - \
		| bedmap --faster --delim "\t" --echo --count - $^ \
		| awk -v OFS="\t" '{ print $$1, $$2, $$3, "i", $$4; }' \
	> $@

$(TMPDIR)/cleavages.sorted.bed: $(TMPDIR)/cleavages.unsorted.bed
	sort-bed --max-mem $(MAX_MEM)G $^ > $@

$(TMPDIR)/fragments.sorted.bed: $(TMPDIR)/fragments.unsorted.bed
	sort-bed --max-mem $(MAX_MEM)G $^ > $@

$(TMPDIR)/fragments.unsorted.bed $(TMPDIR)/cleavages.unsorted.bed: $(INPUT_BAM_FILE)
	samtools view $(SAMTOOLS_FILTER_OPTIONS) $(INPUT_BAM_FILE) \
		| awk -v OFS="\t" ' \
			$$9 < 0 { next; } \
			{ \
				start = $$4-1; \
				end = start + $$9; \
				\
				if(and($$2, 64)) { \
					strand = "+"; \
				} else { \
					strand = "-"; \
				} \
				\
				print $$3, start, end, ".", ".", strand > "$(TMPDIR)/fragments.unsorted.bed"; \
				\
				print $$3, start, start + 1, ".", ".", strand > "$(TMPDIR)/cleavages.unsorted.bed"; \
				print $$3, end, end + 1, ".", ".", strand > "$(TMPDIR)/cleavages.unsorted.bed"; \
			} \
			' \
	> /dev/null