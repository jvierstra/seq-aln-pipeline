#!/bin/env python

import sys

import pysam

import numpy as np

#import pandas as pd

comp = { 'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C', 'N': 'N' }
base_to_index = {'A': 0, 'T': 1, 'C': 2, 'G': 3, 'N': 4 }

def complement(seq):
    return ''.join(comp[base] for base in seq)
     

l = 36

read_lengths = np.ones(l+1)
read_lengths_mapped = np.zeros(l+1)
fragment_lengths = np.zeros(750)

bias_5p = np.zeros((l, 5))
bias_3p = np.zeros((l, 5))
bias_umi = np.zeros((l, 5))

gc_content = np.zeros(101)

reads = pysam.AlignmentFile(sys.argv[1], "rb")

i = 0

for read in reads:
	
	i += 1
	if i > 10000: break

	read_len = len(read.query_sequence)

	read_lengths[read_len] += 1

	if not read.is_proper_pair or read.is_duplicate: continue

	read_lengths_mapped[read_len] += 1

	# orient sequence

	if read.is_reverse:
		seq = complement(read.query_sequence)[::-1] if read.is_read1 else read.query_sequence[::-1] 
	else:		
		seq = read.query_sequence if read.is_read1 else complement(read.query_sequence)

	# try to extract umi
	
	try:
		umi = read.opt("XD")
	except:
		umi = None
		pass
	
	#print umi

	gc = 0.0

	# iterate over seq

	for j in xrange(read_len):

		if seq[j] == "G" or seq[j] == "C":
			gc += 1.0

		if read.is_read1:
			bias_5p[j, base_to_index[seq[j]]] += 1
		else:
			bias_3p[j, base_to_index[seq[j]]] += 1

		if umi and j < 8:
			bias_umi[j, base_to_index[umi[j]]] += 1

	gc_content[np.floor((gc / read_len) * 100)] += 1

	# fragments lengths

	try:
		if read.template_length > 0:
			fragment_lengths[read.template_length] += 1
	except:
		pass
	
# hack

#read_lengths[35] = np.nan
#read_lengths_mapped[35] = np.nan


import matplotlib 
matplotlib.use("Agg")

font = {'family' : 'arial',
        'size'   : 8}

matplotlib.rc('font', **font)

import matplotlib.pyplot as plt  
import matplotlib.gridspec as gridspec

gs = gridspec.GridSpec(4, 2, width_ratios = [1, 1, 0.4])


ax = plt.subplot(gs[0, 0])


ax.grid(b= True, which = 'major', color = 'grey', linestyle = '--')
ax.plot(np.cumsum(read_lengths)/np.sum(read_lengths), 'r')

cumsum_mapped = np.cumsum(read_lengths_mapped)/np.sum(read_lengths)

ax.plot(cumsum_mapped, 'b')
ax.annotate("%0.2f\naligned" % (cumsum_mapped[-1]), xy = (36, cumsum_mapped[-1]), xytext = (25, 0.6),  arrowprops = dict(facecolor = "black", arrowstyle = "->"))
ax.set_xlim(6, 38)
ax.set_ylim(0, 1)
ax.set_xlabel("trimmed tag length")
ax.set_ylabel("cum. fraction\nof tags")


ax = plt.subplot(gs[0, 1])
ax.grid(b= True, which = 'major', color = 'grey', linestyle = '--')
ax.plot(read_lengths_mapped/read_lengths, 'b')
ax.set_xlim(6, 38)
ax.set_ylim(0, 1)
ax.set_xlabel("trimmed tag length")
ax.set_ylabel("fraction of\ntags aligned")

ax = plt.subplot(gs[1, 0])
ax.grid(b= True, which = 'major', color = 'grey', linestyle = '--')
ax.plot(range(1, 37), bias_5p/np.sum(bias_5p, axis = 1)[:, np.newaxis])
ax.set_xlim(0, 37)
ax.set_ylim(0, 0.5)
ax.set_xlabel("base position (from 5' end)")
ax.set_ylabel("fraction at each position")

print bias_3p/np.sum(bias_3p, axis = 1)[:, np.newaxis]

ax = plt.subplot(gs[1, 1])
ax.grid(b= True, which = 'major', color = 'grey', linestyle = '--')
ax.plot(range(1, 37), bias_3p/np.sum(bias_3p, axis = 1)[:, np.newaxis])
ax.set_xlim(37, 0)
ax.set_ylim(0, 0.5)
ax.set_xlabel("base position (from 3' end)")
ax.set_ylabel("fraction at each position")

ax = plt.subplot(gs[2, 0])
ax.grid(b= True, which = 'major', color = 'grey', linestyle = '--')
ax.plot(range(1, 37), bias_umi/np.sum(bias_umi, axis = 1)[:, np.newaxis])
ax.set_xlim(37, 0)
ax.set_ylim(0, 0.75)
ax.set_xlabel("base position (from 5' UMI)")
ax.set_ylabel("fraction at each position")

ax = plt.subplot(gs[2, 1])
ax.grid(b= True, which = 'major', color = 'grey', linestyle = '--')
ax.plot(gc_content)
ax.set_xlabel("G+C content (%)")
ax.set_ylabel("num. tags")

ax = plt.subplot(gs[3, 0])
ax.grid(b= True, which = 'major', color = 'grey', linestyle = '--')
ax.plot(fragment_lengths)
ax.set_xlim(0, 250)
ax.set_xlabel("inferred fragment length")
ax.set_ylabel("num. fragments")


ax = plt.subplot(gs[3, 1])
ax.grid(b= True, which = 'major', color = 'grey', linestyle = '--')

fragment_lengths_cs = np.cumsum(fragment_lengths)/np.sum(fragment_lengths)
lower = np.where(fragment_lengths_cs > 0.25)[0][0]
mid = np.where(fragment_lengths_cs > 0.5)[0][0]
upper = np.where(fragment_lengths_cs > 0.75)[0][0]

print lower, mid, upper

ax.axvline(lower, color = 'black')
ax.axvline(mid, color = 'black')
ax.axvline(upper, color = 'black')

ax.plot(fragment_lengths_cs)

ax.set_xlim(0, 250)

ax.set_xlabel("inferred fragment length")
ax.set_ylabel("cum. fraction\nof fragments")

plt.tight_layout()

plt.savefig("out.svg")

