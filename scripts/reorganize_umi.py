#!/bin/env python

import sys
import gzip

outfile = gzip.open(sys.argv[2], 'wb')

for line in gzip.open(sys.argv[1]):

	line = line.strip()

	if line[0] == "@":		

		insert_loc = line.find(' ')
		
		umi_loc = line.find('+') + 1
		umi = line[umi_loc:] if umi_loc != 0 else ""

		line = line[:insert_loc] + "#" + umi + line[insert_loc:]

	outfile.write(line + '\n')

outfile.close()
